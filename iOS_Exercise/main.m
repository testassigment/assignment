//
//  main.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
