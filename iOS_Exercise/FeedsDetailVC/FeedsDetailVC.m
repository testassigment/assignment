//
//  FeedsDetailVC.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "FeedsDetailVC.h"

@interface FeedsDetailVC ()

@end

@implementation FeedsDetailVC

#pragma mark - View LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Memory Management Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
