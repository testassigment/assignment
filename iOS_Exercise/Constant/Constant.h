//
//  Constant.h
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define BASE_URL        @"https://dl.dropboxusercontent.com/u/29165357/feed.json/"
#define APP_DELEGATE    ((AppDelegate *)[UIApplication sharedApplication].delegate)

#endif /* Constant_h */
