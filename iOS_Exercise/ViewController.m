//
//  ViewController.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "ViewController.h"
#import "FeedsCell.h"
#import "Constant.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import <RKEntityMapping.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Feeds.h"
#import "FeedsList.h"
#import "FeedsEntity.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
    /**
     @var feedsArray
     @description Array to store feed response received from the api
     */
    NSArray *feedsArray;
}

/**
 @property tblFeeds
 @description UITableView to show feeds
 */
@property (weak, nonatomic) IBOutlet UITableView *tblFeeds;
@end

@implementation ViewController

#pragma mark - View LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupInitialLayoutSettings];
}

#pragma mark - Other Methods

/**
 @method setupInitialLayoutSettings
 @description Method to set initial layout settings and default values
 */
- (void)setupInitialLayoutSettings {
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(refreshData)];
    self.navigationItem.rightBarButtonItem = btnRefresh;
    
    self.tblFeeds.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    //Register cell for tableView
    [self.tblFeeds registerNib:[UINib nibWithNibName:@"FeedsCell" bundle:nil] forCellReuseIdentifier:@"FeedCellIdentifier"];
    
    [self checkIfFeedsAlreadyAvailable];
}

/**
 @method checkIfFeedsAlreadyAvailable
 @description This method will check if feeds files is already downloaded in document directory or not
 */
- (void)checkIfFeedsAlreadyAvailable {
    
    //Check if file exists in document directory or not
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"]];
    if (!fileExists) {
        [self downloadFile];
    }
    else {
        
        [self mapDownloadedFeeds];
    }
}

/**
 @method refreshData
 @description Method to get updated response from api
 */
- (void)refreshData {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"] error:&error];
        if (success) {
            
            [self checkIfFeedsAlreadyAvailable];
        }
    });
}

/**
 @method getDocumentDirectoryReference
 @description Method to get the document directory reference path
 @returns Path of the document directory
 */
- (NSString *)getDocumentDirectoryReference {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

/**
 @method downloadFile
 @description This method will download the json file from the url and save into the document directory
 */
- (void)downloadFile {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:BASE_URL]];
    AFHTTPRequestOperation *downloadRequest = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [downloadRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //If response received is succes,write the response in to the document directory path
        if (responseObject) {
            
            NSData *data = [[NSData alloc] initWithData:responseObject];
            NSString *filePath = [[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"];
            NSLog(@"File path is %@",filePath);
            [data writeToFile:filePath atomically:YES];
            
            [self mapDownloadedFeeds];
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to download the file." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"file downloading error : %@", [error localizedDescription]);
    }];
    
    // Step 5: begin asynchronous download
    [downloadRequest start];
}

/**
 @method mapDownloadedFeeds
 @description This method will start the Mapping process and write the data into the core data instance
 */
- (void)mapDownloadedFeeds {
    
    //Get the reference of the RKObjectManager class, configured with the BASE URL
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:BASE_URL]];
    
    //Get the downloaded file from the document directory for initiating the mapping process
    NSString *feedsResponse = [[NSString alloc] initWithContentsOfFile:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"] encoding:NSUTF8StringEncoding error:NULL];
    NSString *MIMEType = @"application/json";
    NSError *parsingError = nil;
    
    //Convert the data into the JSON object with encoding
    NSData *data = [feedsResponse dataUsingEncoding:NSUTF8StringEncoding];
    id parsedData = [RKMIMETypeSerialization objectFromData:data MIMEType:MIMEType error:&parsingError];
    if (parsedData == nil && parsingError) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to parse the response." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    //Get the reference on the managed object instance and assign to RKObjectManager
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FeedsModel" ofType:@"momd"]];
    NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
 
    objectManager.managedObjectStore = managedObjectStore;
    
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"FeedsEntity" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[@"title"];
    [feedMapping addAttributeMappingsFromArray:@[@"title"]];
    
    //Start the mapping process with reference to the model class for Title
    RKEntityMapping *feedsListMapping = [RKEntityMapping mappingForEntityForName:@"FeedsListEntity" inManagedObjectStore:managedObjectStore];
    feedsListMapping.identificationAttributes = @[@"image"];
    [feedsListMapping addAttributeMappingsFromDictionary:@{
                                                      @"title": @"title",
                                                      @"description": @"feedsdescription",
                                                      @"imageHref": @"image"
                                                      }];
    
    [feedMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"feeds" toKeyPath:@"root" withMapping:feedsListMapping]];
    
    
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[FeedsEntity class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"title":   @"title"
                                                  }];
   RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor
     responseDescriptorWithMapping:mapping
     method:RKRequestMethodGET
     pathPattern:@"title"
     keyPath:@""
     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
   // RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:@"" keyPath:@"" statusCodes:nil];
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
    NSURL *url = [NSURL URLWithString:BASE_URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"The public timeline Tweets: %@", [result array]);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        NSLog(@"Failure: %@",[error localizedDescription]);

    }];
    [operation start];
    
//    RKResponseDescriptor *statusResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:feedMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"title" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
//    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
//
//    NSURL *url = [NSURL URLWithString:BASE_URL];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[statusResponseDescriptor]];
//    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
//        NSLog(@"SUCCESS: %@", [result array]);
//    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
//        NSLog(@"FAILURE: %@", [error localizedDescription]);
//        
//        RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
//
//    }];
//    [operation start];

    // Register article mapping with the provider
//    RKResponseDescriptor *articleListResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:feedMapping
//                                                                                                       method:RKRequestMethodGET
//                                                                                                  pathPattern:@"/v1/categories/:id/articles.json"
//                                                                                                      keyPath:nil
//                                                                                                  statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    //[objectManager addResponseDescriptor:statusResponseDescriptor];
    
    return;
    
    NSString *seedPath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"FeedsModel.sqlite"];
    RKManagedObjectImporter *importer = [[RKManagedObjectImporter alloc] initWithManagedObjectModel:managedObjectStore.managedObjectModel storePath:seedPath];
    
    NSError *error;
    [importer importObjectsFromItemAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"]
                              withMapping:feedMapping
                                  keyPath:@""
                                    error:&error];
    //Check if response is successfully written into the code data instance or not
    BOOL success = [importer finishImporting:&error];
    
    if (success) {
        
        [importer logSeedingInfo];
        
//        [importer importObjectsFromItemAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"]
//                                  withMapping:feedsListMapping
//                                      keyPath:@"rows"
//                                        error:&error];
//        //Check if response is successfully written into the code data instance or not
//        BOOL successFeeds = [importer finishImporting:&error];
//        
//        if (successFeeds) {
//            
//            [importer logSeedingInfo];
//        }
//        else {
//            
//            NSLog(@"error localized description == %@", [error localizedDescription]);
//        }

        
    }
    else {
        
        NSLog(@"error localized description == %@", [error localizedDescription]);
    }
    
    
    
    feedsArray = [NSArray array];
    feedsArray = [self getFeedsFromDatabase];
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [self.tblFeeds reloadData];
    });
}
/**
 @method getFeedsDatabase
 @description This method will get the saved feed instances from the core data manage object context
 @returns Returns the array of the feeds from the db
 */

- (NSArray *)getFeedsFromDatabase {
    
    NSArray *recordArray = [NSArray array];
   NSManagedObjectContext *managedObjectContext = [APP_DELEGATE managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"FeedsEntity"];
    NSLog(@"fetch request is: %@", fetchRequest);

    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    fetchRequest.sortDescriptors = @[descriptor];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    Feeds *feeds = [fetchedObjects firstObject];
    NSLog(@"data is %@",[feeds valueForKey:@"title"]);
    
    if ([feeds valueForKey:@"title"] != nil) {
        self.navigationItem.title = [feeds valueForKey:@"title"];
    }
    
    NSLog(@"Count is %ld",feeds.feedsList.count);
    for (NSManagedObject *records in fetchedObjects) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:0];
        [dict setValue:[records valueForKey:@"title"] forKey:@"title"];
        [dict setValue:[records valueForKey:@"feedsdescription"] forKey:@"feedsdescription"];
        [dict setValue:[records valueForKey:@"image"] forKey:@"image"];
       // [recordArray addObject:dict];
    }
    return recordArray;
    
    
//    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
//    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"ArticleList"];
//    
//    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
//    fetchRequest.sortDescriptors = @[descriptor];
//    
//    NSError *error = nil;
//    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
//    
//    ArticleList *articleList = [fetchedObjects firstObject];
//    
//    self.articles = [articleList.articles allObjects];
}


#pragma mark - UITableViewDataSource & Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [feedsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"FeedCellIdentifier";
    
    FeedsCell *cell = (FeedsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"FeedsCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if ([feedsArray count] > 0) {
        
        cell.lblFeedTittle.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"title"];
        cell.lblFeedDescription.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"feedsdescription"];
        
        if ([[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"] length] > 0) {
            
            NSLog(@"Image url is %@",[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"]);
            
            [cell.imgVwFeedImage sd_setImageWithURL:[NSURL URLWithString:[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"]] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image){
                    cell.imgVwFeedImage.image = image;
                }
            }];
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FeedsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCellIdentifier"];
    
    if (cell == nil) {
        
        cell = (FeedsCell *)[[[NSBundle mainBundle] loadNibNamed:@"FeedsCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.lblFeedDescription.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"feedsdescription"];
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tblFeeds.frame), CGRectGetHeight(cell.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}


#pragma mark - Memory Management Methods

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
