//
//  Feeds.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/23/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "Feeds.h"
#import "FeedsList.h"

@implementation Feeds

@dynamic feedsTitle;
@dynamic feedsList;

@end
