//
//  FeedsListEntity.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/23/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "FeedsListEntity.h"

@implementation FeedsListEntity

@dynamic title;
@dynamic feedsdescription;
@dynamic image;

@end
