//
//  Feeds.h
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/23/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import <CoreData/CoreData.h>

@class FeedsList;

@interface Feeds : NSManagedObject

@property (nullable, nonatomic, retain) NSString *feedsTitle;
@property (nullable,nonatomic, retain) NSSet *feedsList;

@end
