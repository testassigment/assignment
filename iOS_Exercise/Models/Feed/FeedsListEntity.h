//
//  FeedsListEntity.h
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/23/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface FeedsListEntity : NSManagedObject

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *feedsdescription;
@property (nullable, nonatomic, retain) NSString *image;

@end
