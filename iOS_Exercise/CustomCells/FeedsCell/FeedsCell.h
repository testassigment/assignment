//
//  FeedsCell.h
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedsCell : UITableViewCell

/**
 @property lblFeedTittle
 @description UILabel to show feed tittle
 */
@property(nonatomic, weak) IBOutlet UILabel *lblFeedTittle;

/**
 @property lblFeedDescription
 @description UILabel to show feed description
 */
@property(nonatomic, weak) IBOutlet UILabel *lblFeedDescription;

/**
 @property imgVwFeedImage
 @description UIImageView to show Image associated with the feed
 */
@property(nonatomic, weak) IBOutlet UIImageView *imgVwFeedImage;
@end
